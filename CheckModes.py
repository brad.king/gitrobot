#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
from Utility import *

class CheckModes:
    def __init__(self, exe_exts = None):
        if exe_exts is None:
            exe_exts = ('.bat', '.cmd', '.exe', '.com')
        self._exe_exts = exe_exts

    def _looks_executable(self, name, obj):
        if name.endswith(self._exe_exts):
            return True
        content = git('cat-file', 'blob', obj)
        return content.startswith('#!/') or \
               content.startswith('#! /')

    def enforce(self, commit, user):
        failures = []

        # Check mode changes.
        for diff in commit.diffs():
            src_mode = diff['src_mode']
            dst_mode = diff['dst_mode']
            if dst_mode == src_mode or dst_mode == '000000':
                continue
            name = diff['name']
            if dst_mode == '100755':
                if not self._looks_executable(name, diff['dst_obj']):
                    failures.append('commit %s adds\n  %s\nwith executable mode, but the file does not look executable' % (commit.sha1[0:8], name))
            elif dst_mode == '100644':
                if self._looks_executable(name, diff['dst_obj']):
                    failures.append('commit %s adds\n  %s\nwithout executable mode, but the file looks executable' % (commit.sha1[0:8], name))
            elif dst_mode != '160000':
                failures.append('commit %s adds\n  %s\nwith non-file mode %s' % (commit.sha1[0:8], name, dst_mode))

        if failures:
            fail('\n'.join(failures))
