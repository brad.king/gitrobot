#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import os
import sys
import argparse
import subprocess
from cStringIO import StringIO
from Commit import Commit
from Git import Git
from User import User
from Utility import *

class GerritError(Exception):
    def __init__(self, value): self.value = value
    def __str__(self): return repr(self.value)

class Gerrit:
    def __init__(self, parser, argv = sys.argv[1:], git = Git(),
                 username = 'kwrobot', host = None, port = 29418):
        self.args, _ = parser.parse_known_args(argv)
        if 'uploader' in self.args:
          self.uploader = User(self.args.uploader)
        else:
          self.uploader = None
        self.git = git
        self.username = username
        self.host = host or os.environ.get('GERRIT_REVIEW_HOST','localhost')
        self.port = str(port)

    def run_checks(self, checks, rev):
        commit = Commit(rev)
        for c in checks:
            c.enforce(commit, self.uploader)

    def indent(self, text, indent = ' '):
        return ''.join([indent+line for line in StringIO(text)])

    def review(self, verified = None, message = None, topic = False):
        args = ['ssh', '-p', self.port, '%s@%s' % (self.username, self.host),
                'gerrit', 'review', '--project', self.args.project]
        if verified:
            args.extend(['--verified=' + verified])
        if message:
            args.extend(['--message',"'%s'" % message.replace("'","""'"'"'""")])
        if topic:
            args.extend(['--topic'])
        args.append(self.args.commit)
        p = subprocess.Popen(args)
        p.communicate()

    def query(self, *args, **keys):
        argv = ['ssh', '-p', self.port, '%s@%s' % (self.username, self.host),
                'gerrit', 'query']
        argv.extend(args)
        p = subprocess.Popen(argv, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        result = p.communicate()
        if keys.get('safe', False):
            return p.returncode, result[0], result[1]
        elif p.returncode != 0:
            raise GerritError(result[1])
        else:
            return result[0]


class GerritPatchsetCreated(Gerrit):
    def __init__(self, **keys):
        parser = argparse.ArgumentParser(description='patchset-created')
        parser.add_argument('--change', metavar='<change id>', required=True)
        parser.add_argument('--change-url', metavar='<change url>', required=True)
        parser.add_argument('--change-owner', metavar='<change owner>', required=True)
        parser.add_argument('--project', metavar='<project name>', required=True)
        parser.add_argument('--branch', metavar='<branch>', required=True)
        parser.add_argument('--uploader', metavar='<uploader>', required=True)
        parser.add_argument('--commit', metavar='<commit>', required=True)
        parser.add_argument('--patchset', metavar='<patchset id>', required=True)
        parser.add_argument('--is-draft', metavar='<is draft>', required=False)
        parser.add_argument('--topic', metavar='<topic>', required=False)
        parser.add_argument('--kind', metavar='<kind>', required=False)
        Gerrit.__init__(self, parser, **keys)

    def check(self, checks, soft_checks = []):
        try:
            self.run_checks(checks, self.args.commit)
            verified = '+1'
            message = 'Commit %s passes basic content checks.' % self.args.commit[0:8]
        except Fail, f:
            verified = '-1'
            message = 'Error:\n' + self.indent(str(f))
        for check in soft_checks:
            try:
                self.run_checks([check], self.args.commit)
            except Fail, f:
                message = message + '\n\nWarning:\n' + self.indent(str(f))
        self.review(verified, message)

class GerritCommentAdded(Gerrit):
    def __init__(self, **keys):
        parser = argparse.ArgumentParser(description='comment-added')
        parser.add_argument('--change', metavar='<change id>', required=True)
        parser.add_argument('--change-url', metavar='<change url>', required=True)
        parser.add_argument('--change-owner', metavar='<change owner>', required=True)
        parser.add_argument('--project', metavar='<project name>', required=True)
        parser.add_argument('--branch', metavar='<branch>', required=True)
        parser.add_argument('--author', metavar='<author>', required=True)
        parser.add_argument('--commit', metavar='<commit>', required=True)
        parser.add_argument('--comment', metavar='<comment>', required=True)
        parser.add_argument('--is-draft', metavar='<is draft>', required=False)
        parser.add_argument('--topic', metavar='<topic>', required=False)
        Gerrit.__init__(self, parser, **keys)

class GerritChangeMerged(Gerrit):
    def __init__(self, **keys):
        parser = argparse.ArgumentParser(description='change-merged')
        parser.add_argument('--change', metavar='<change id>', required=True)
        parser.add_argument('--change-url', metavar='<change url>', required=True)
        parser.add_argument('--change-owner', metavar='<change owner>', required=True)
        parser.add_argument('--project', metavar='<project name>', required=True)
        parser.add_argument('--branch', metavar='<branch>', required=True)
        parser.add_argument('--submitter', metavar='<submitter>', required=True)
        parser.add_argument('--commit', metavar='<commit>', required=True)
        parser.add_argument('--topic', metavar='<topic>', required=False)
        Gerrit.__init__(self, parser, **keys)

class GerritChangeAbandoned(Gerrit):
    def __init__(self, **keys):
        parser = argparse.ArgumentParser(description='change-abandoned')
        parser.add_argument('--change', metavar='<change id>', required=True)
        parser.add_argument('--change-url', metavar='<change url>', required=True)
        parser.add_argument('--change-owner', metavar='<change owner>', required=True)
        parser.add_argument('--project', metavar='<project name>', required=True)
        parser.add_argument('--branch', metavar='<branch>', required=True)
        parser.add_argument('--topic', metavar='<topic>', required=False)
        parser.add_argument('--abandoner', metavar='<abandoner>', required=True)
        parser.add_argument('--commit', metavar='<commit>', required=True)
        parser.add_argument('--reason', metavar='<reason>', required=True)
        Gerrit.__init__(self, parser, **keys)

class GerritChangeRestored(Gerrit):
    def __init__(self, **keys):
        parser = argparse.ArgumentParser(description='change-restored')
        parser.add_argument('--change', metavar='<change id>', required=True)
        parser.add_argument('--change-url', metavar='<change url>', required=True)
        parser.add_argument('--change-owner', metavar='<change owner>', required=True)
        parser.add_argument('--project', metavar='<project name>', required=True)
        parser.add_argument('--branch', metavar='<branch>', required=True)
        parser.add_argument('--topic', metavar='<topic>', required=False)
        parser.add_argument('--restorer', metavar='<restorer>', required=True)
        parser.add_argument('--commit', metavar='<commit>', required=True)
        parser.add_argument('--reason', metavar='<reason>', required=True)
        Gerrit.__init__(self, parser, **keys)

class GerritUpdate(Gerrit):
    def __init__(self, **keys):
        parser = argparse.ArgumentParser(description='update')
        parser.add_argument('--project', metavar='<project name>')
        parser.add_argument('--uploader', metavar='<uploader>', required=True)
        parser.add_argument('--refname', metavar='<refname>', required=True)
        parser.add_argument('--oldrev', metavar='<oldrev>', required=True)
        parser.add_argument('--newrev', metavar='<newrev>', required=True)
        Gerrit.__init__(self, parser, **keys)
        # Identify the branch for which new commits are meant.
        if self.args.refname.startswith('refs/heads/'):
            self.branch = self.args.refname[11:]
        elif self.args.refname.startswith('refs/for/'):
            branch = self.args.refname[9:]
            if '/' in branch:
                self.branch = branch.split('/',1)[0]
            else:
                self.branch = branch
        else:
            self.branch = None

    def newrevs(self):
        if self.args.newrev == zero:
            return []
        revs = self.git('rev-list', '--reverse', '--topo-order',
                        self.args.newrev, '--not', '--all')
        return revs.splitlines()

    def deny_heads_by_refs(self, refs_deny = 'refs/deny/', ns = None):
        """Deny changes for <head> that contain any refs/deny/<head>/*"""
        if not self.branch:
            return
        ns = ns or self.branch
        refs = self.git('for-each-ref',
                        '--format=%(objectname) %(refname:short)',
                        refs_deny+ns+'/')
        newrev = self.args.newrev
        for line in refs.splitlines():
            rev, ref = line.split()
            code, out, err = self.git('merge-base', rev, newrev, safe=True)
            if code == 0 and out.strip() == rev:
                die("""denying commit %s which contains %s (%s)""" %
                    (newrev[0:8], ref, rev[0:8]))

    def deny_bad_heads(self, bad = []):
        newrev = self.args.newrev
        for rev in bad:
            code, out, err = self.git('merge-base', rev, newrev, safe=True)
            if code == 0 and out.strip() == rev:
                die('commit %s is a known bad commit that was removed from the server' %
                    rev[0:8])

    def check(self, checks):
        try:
            for rev in self.newrevs():
                self.run_checks(checks, rev)
        except Fail, f:
            die(str(f))
