Kitware Git Server-side Checks
==============================

The `githooks` branch provides scripts meant for use in Git server-side checks.
Example usage in a Git `update` hook:

    import githooks
    update = githooks.Update()
    checks = [
        githooks.CheckPerson(),
        githooks.CheckMerge(allow_merge = True, allow_root = False),
        githooks.CheckModes(),
        githooks.CheckSize(),
        githooks.CheckCRLF(),
        ]
    update.check(checks)
