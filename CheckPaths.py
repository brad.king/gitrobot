#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
from Utility import *
import re

class CheckPaths:
    def __init__(self, bad_chars = '<>:"|?*'):
        self.git = git
        self._bad_chars = bad_chars
        self._bad_regex = re.compile('['+re.escape(bad_chars or '')+']')
    def enforce(self, commit, user):
        bad_paths = ''
        for diff in commit.diffs():
            name = diff['name']
            src_obj = diff['src_obj']
            if src_obj == zero and self._bad_regex.search(name):
                bad_paths = bad_paths + '\n  ' + name
        if bad_paths:
            die("""commit %s adds paths:%s
containing at least one forbidden character: %s""" %
                (commit.sha1[0:8], bad_paths, self._bad_chars))
