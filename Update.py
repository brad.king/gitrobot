#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import sys
from cStringIO import StringIO
from Commit import Commit
from Git import Git
from User import User
from Utility import *

class Update:
    def __init__(self, argv = sys.argv, maint = [], admin = [],
                 git = Git(), no_exclude = {}, explicit_revlist = False):
        self.user = User() or die('anonymous access not allowed')
        if self.user.email == 'admin' or self.user.email in admin:
            sys.exit(0)
        self.maint = self.user.email in maint
        self.git = git
        self._no_exclude = no_exclude
        self._check_command_line(argv)
        if explicit_revlist:
            self._revlist = argv[4:]
            if not self._revlist:
                die('empty explicit revlist')

    def _check_command_line(self, argv):
        """Used by constructor to extract command line options."""
        if len(argv) < 4:
            die('Usage: %s <refname> <oldrev> <newrev>\n' % argv[0])
        self.refname = argv[1]
        self.oldrev = argv[2]
        self.newrev = argv[3]
        if self.oldrev == self.newrev:
            die('trivial update not allowed')
        type_obj = self.newrev
        if self.newrev == zero:
            self.action = 'delete'
            type_obj = self.oldrev
        elif self.oldrev == zero:
            self.action = 'create'
        else:
            self.action = 'update'
        self.type = git('cat-file', '-t', type_obj).strip()

    def _load_revlist(self):
        # No new revisions during branch deletion.
        if self.newrev == zero:
            self._revlist = []
            return

        # Commits new to this branch.
        if self.oldrev == zero:
            revspec = self.newrev
        else:
            revspec = self.oldrev+'..'+self.newrev

        # Commits already on other branches.
        heads = self.git('for-each-ref', '--format=%(refname)', 'refs/heads/')
        heads = [ref.strip() for ref in StringIO(heads)
                 if not ref.strip() == self.refname]
        if self._no_exclude.has_key(self.refname):
                no_exclude = self._no_exclude[self.refname]
                heads = list(set(heads) - set(no_exclude))
        exclude = self.git('rev-parse', '--not', *heads)

        # Commits new to the repository.
        revs = self.git('rev-list', '--reverse', '--topo-order', '--stdin',
                        revspec, stdin = exclude)
        self._revlist = revs.splitlines()

    def revlist(self):
        """Get list of new revisions."""
        if not hasattr(self, '_revlist'):
            self._load_revlist()
        return self._revlist

    def _load_commits(self):
        self._commits = []
        for rev in self.revlist():
            self._commits.append(Commit(rev))

    def commits(self):
        """Get list of new commits."""
        if not hasattr(self, '_commits'):
            self._load_commits()
        return self._commits

    def deny_heads_by_refs(self, refs_deny, ns = None):
        """Deny update of <head> to contain any refs/deny/<head>/*"""
        if (self.action == 'delete' or
            not self.refname.startswith('refs/heads/')):
            return
        head = self.refname[11:]
        ns = ns or head
        refs = self.git('for-each-ref',
                        '--format=%(objectname) %(refname:short)',
                        refs_deny+ns+'/')
        for line in refs.splitlines():
            rev, ref = line.split()
            code, out, err = self.git('merge-base', rev, self.newrev, safe=True)
            if code == 0 and out.strip() == rev:
                die("""denying %s (%s) which contains %s (%s)""" %
                    (head, self.newrev[0:8], ref, rev[0:8]))

    def deny_non_fast_forward(self):
        """Deny non-fast-forward updates."""
        if git('merge-base', self.newrev, self.oldrev).strip() != self.oldrev:
            die("""non-fast-forward update not allowed.""")

    def deny_non_linear_merges(self):
        """Deny updates adding poorly shaped history."""
        if not self.action == 'update': return
        # The old revision must be reachable by a first-parent traversal.
        revs = git('rev-list', '--reverse', '--first-parent',
                   self.oldrev + '..' + self.newrev)
        first, rest = revs.split('\n', 1)
        base = git('rev-parse', first+'^1').strip()
        if base != self.oldrev:
            die("""first-parent sequence not preserved.""")

    def _check_topic_base(self, topic, badbase):
        code, bases, err = git_safe('merge-base', '--all', self.oldrev, topic)
        if code != 0: return
        for base in bases.splitlines():
            code, out, err = git_safe('merge-base', badbase, base)
            if code == 0 and out.strip() == badbase:
                die("""topics must be merged into %s, not based on it.

Commit %s is based on %s at commit %s.""" %
                    (self.refname, topic[0:8], self.refname, base[0:8]))

    def deny_non_merge_sequence(self, badbases = []):
        """Deny updates adding poorly shaped history."""
        if not self.action == 'update': return
        # A first-parent traversal must see only merges.
        revs = git('rev-list', '--reverse', '--first-parent',
                   self.oldrev + '..' + self.newrev)
        for rev in revs.splitlines():
            code, out, err = git_safe('rev-parse', rev+'^2')
            if code != 0:
                die("""topics must be merged into %s, not based on it.

Commit %s appears in a first-parent traversal but is not a merge.""" %
                    (self.refname, rev[0:8]))
            for base in badbases:
                self._check_topic_base(out.strip(), base)

    # Handy command for finding a 'non_release_root':
    #   git log --first-parent --reverse --pretty=oneline \                 #
    #       "$( git merge-base --fork-point release master )"..master | \   #
    #   head -n1                                                            #
    def check_for_release_branches(self, branches):
        """Check the branch for release eligibility."""
        if not self.action == 'update': return
        for name, non_release_root in branches:
            base = git('merge-base', non_release_root, self.newrev)
            base = base.strip()
            if not base == non_release_root:
                info('Eligible for the %s release branch.' % name)

    def check_always(self, commit):
        maxsize = 8192
        if len(commit.log) > maxsize:
            die('commit %s has a commit message larger than %d bytes' %
                (commit.sha1[0:8], maxsize))

    def check(self, checks):
        try:
            for commit in self.commits():
                for c in checks:
                    c.enforce(commit, self.user)
                self.check_always(commit)
        except Fail, f:
            die(str(f))

    def check_all(self, checks):
        errs = []
        for commit in self.commits():
            for c in checks:
                try:
                    c.enforce(commit, self.user)
                except Fail, f:
                    errs.append(str(f))
            self.check_always(commit)
        if errs:
            die_plain('\n\n'.join(errs))
